package com.buggy;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.web.reactive.HandlerMapping;
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketSession;
import org.springframework.web.reactive.socket.server.WebSocketService;
import org.springframework.web.reactive.socket.server.support.HandshakeWebSocketService;
import org.springframework.web.reactive.socket.server.support.WebSocketHandlerAdapter;
import org.springframework.web.reactive.socket.server.upgrade.ReactorNettyRequestUpgradeStrategy;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
@Slf4j
public class MessageServerApplication {

   /* @Autowired
    private WebSocketHandler webSocketHandler;*/

    /*	@Bean
        WebSocketHandlerAdapter webSocketHandlerAdapter() {
            return new WebSocketHandlerAdapter();
        }

        WebSocketHandler webSocketHandler() {
            return session ->
                    session.send(
                            Flux.interval(Duration.ofSeconds(1))
                                    .map(n -> n.toString())
                                    .map(session::textMessage)
                    ).and(session.receive()
                            .map(WebSocketMessage::getPayloadAsText)
                            .doOnNext(msg -> log.info("Prime#: " + msg))
                    );
        }*/
/*

    @Bean
    public HandlerMapping webSocketHandlerMapping() {
        Map<String, WebSocketHandler> map = new HashMap<>();
        map.put("/event-emitter", webSocketHandler);

        SimpleUrlHandlerMapping handlerMapping = new SimpleUrlHandlerMapping();
        handlerMapping.setOrder(1);
        handlerMapping.setUrlMap(map);
        return handlerMapping;
    }
*/
    @Bean
    public WebSocketHandler echoHandler() {
        return new WebSocketHandler() {
            @Override
            public Mono<Void> handle(WebSocketSession session) {
                return session
                        .send(session.receive()
                                .map(msg -> "RECEIVED ON SERVER :: " + msg.getPayloadAsText())
                                .map(session::textMessage)
                        );
            }
        };
    }

    @Bean
    public HandlerMapping handlerMapping() {
        Map<String, WebSocketHandler> map = new HashMap<>();
        map.put("/echo", echoHandler());

        SimpleUrlHandlerMapping mapping = new SimpleUrlHandlerMapping();
        mapping.setUrlMap(map);
        mapping.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return mapping;
    }

    @Bean
    public WebSocketHandlerAdapter webSocketHandlerAdapter() {
        return new WebSocketHandlerAdapter();
    }

    @Bean
    public WebSocketService webSocketService() {
        return new HandshakeWebSocketService(new ReactorNettyRequestUpgradeStrategy());
    }

    public static void main(String[] args) {
        SpringApplication.run(MessageServerApplication.class, args);
    }


  /*  @Component
    public class ReactiveWebSocketHandler
            implements WebSocketHandler {

        // private fields ...

        @Override
        public Mono<Void> handle(WebSocketSession webSocketSession) {
            return webSocketSession.send(Flux.interval(Duration.ofSeconds(1))
                         .map(n -> n.toString())
                    .map(webSocketSession::textMessage))
                    .and(webSocketSession.receive()
                            .map(WebSocketMessage::getPayloadAsText)
                            .log());

        }
    }*/
}


